import { Component, ElementRef, ViewChild } from '@angular/core'
import { CdkDragEnd, CdkDragStart } from '@angular/cdk/drag-drop'

import { SocketService } from 'src/app/socket.service'

type item = {'id': Number, 'boardId': String, 'type': String, 'x': Number, 'y': Number, 'text': String, 'active': Boolean, 'editmode': Boolean}

@Component({
  selector: 'app-whiteboard',
  templateUrl: './whiteboard.component.html',
  styleUrls: ['./whiteboard.component.css']
})
export class WhiteboardComponent {

    @ViewChild('whiteboard') 
    whiteboard!: ElementRef;

    boardId = ''
    items!: item[]
    offset = {x: 3, y: 3}
    
    constructor(private socketService: SocketService) {
        this.socketService.listen('board').subscribe(data => (this.initBoard(data)))
        this.socketService.listen('start move item').subscribe(data => (this.startMoveItem(data)))
        this.socketService.listen('end move item').subscribe(data => (this.endMoveItem(data)))
        this.socketService.listen('new item').subscribe(data => (this.newItem(data)))
        this.socketService.listen('update item text').subscribe(data => (this.updateItemText(data)))
    }
    
    getItem(id: any) {
        return this.items.filter(item => {
            return item.id == id
        })[0]
    }

    initBoard(data: any) {
        this.boardId = data.boardId
        this.items = data.items
    }

    // doubleClick(id: any) {
    //     console.log('h')
    //     let item = this.getItem(id)
    //     item.editmode = true
    // }

    // resize(id: any){
    //     console.log('h')
    //     let item = this.getItem(id)
    // }


    dragStart(event: CdkDragStart) {
        this.socketService.emit('start move item', {
            'boardId': this.boardId,
            'id': event.source.element.nativeElement.id
        })
    }

    startMoveItem(data: any) {
        let item = this.getItem(data.id)
        item.active = false
    }

    dragEnd(event: CdkDragEnd) {
        let newPosition = event.source.getRootElement().getBoundingClientRect()
        let relativePosition = this.whiteboard.nativeElement.getBoundingClientRect()
        event.source._dragRef.reset()
        let position = {
            'x': newPosition.x - relativePosition.x - this.offset.x,
            'y': newPosition.y - relativePosition.y - this.offset.y,
        }
        let item = this.getItem(event.source.element.nativeElement.id)
        item.x = position.x
        item.y = position.y
        this.socketService.emit('end move item', {
            'boardId': this.boardId,
            'id': item.id,
            'position': position,
        })
    }

    endMoveItem(data: any) {
        let item = this.getItem(data.id)
        item.x = data.position.x
        item.y = data.position.y
        setTimeout(() => { item.active = true }, 400);
    }
    
    editItem(event: Event) {
        if (event.target) {
            this.socketService.emit('update item text', {
                'boardId': this.boardId,
                'id': (event.target as HTMLTextAreaElement).id,
                'text': (event.target as HTMLTextAreaElement).value,
            })
        }
    }

    updateItemText(data: any) {
        let item = this.getItem(data.id)
        item.text = data.text
    }

    dragEndNew(event: CdkDragEnd, type: String) {
        let position = event.source.element.nativeElement.getBoundingClientRect()
        let relativePosition = this.whiteboard.nativeElement.getBoundingClientRect()
        event.source._dragRef.reset()
        let item = {
            'id': this.items.length,
            'boardId': this.boardId,
            'type': type,
            'x': position.x - relativePosition.x - this.offset.x,
            'y': position.y - relativePosition.y - this.offset.y,
            'text': 'New Note',
            'active': true,
            'editmode': false,
        }
        this.items.push(item)
        this.socketService.emit('new item', {
            'boardId': this.boardId,
            'item': item,
        })
    }

    newItem(data: any) {
        this.items.push(data.item)
    }

}
