export const environment = {
  production: true,
  SOCKET_URI: "https://toomanyteams-socket.herokuapp.com"
};
