var PORT = process.env.PORT || 4444
var ORIGIN = process.env.ORIGIN || "http://localhost:4200"

var express = require ('express')
var app = express()
app.set('port', PORT)

var server = app.listen(app.get('port'), () => {
    console.log('Node app is running on port', app.get('port'))
})

var io = require('socket.io')(server, {
    cors: {
        origin: ORIGIN,
        credentials: true,
        methods: ["GET", "POST"]
    }
})

defaultBoardId = 'board1'

items = [
    {'id': 0, 'boardId': defaultBoardId, 'type': 'box', 'x': 0, 'y': 0, 'text': 'I am a box!', 'active': true, 'editmode': false},
    {'id': 1, 'boardId': defaultBoardId, 'type': 'box', 'x': 200, 'y': 0, 'text': 'I am a box too!', 'active': true, 'editmode': false},
]

function getItem(id) {
    return items.find(item => {
        return item.id == id
    })
}

io.on('connection', (socket) => {
    console.log('A user connected to', defaultBoardId)

    socket.join(defaultBoardId)
    socket.emit('board', {'boardId': defaultBoardId, 'items': items})

    socket.on("start move item", (data) => {
        console.log('start move item:', data)
        
        let item = getItem(data.id)

        if (item !== undefined) {
            item.active = false

            socket.to(data.boardId).emit("start move item", data)
        } else {
            socket.emit('board', {'boardId': defaultBoardId, 'items': items})
        }
    })

    socket.on("end move item", (data) => {
        console.log('end move item:', data)

        let item = getItem(data.id)

        if (item !== undefined) {
            item.active = true
            item.x = data.position.x
            item.y = data.position.y
                
            socket.to(data.boardId).emit("end move item", data)
        } else {
            socket.emit('board', {'boardId': defaultBoardId, 'items': items})
        }
    })

    socket.on("update item text", (data) => {
        let item = getItem(data.id)
        item.text = data.text

        socket.to(data.boardId).emit("update item text", data)
    })

    socket.on("new item", (data) => {
        console.log('new item:', data)

        items.push(data.item)

        socket.to(data.boardId).emit("new item", data)
    })

    socket.on('disconnect', () => {
        console.log('A user disconnected from', defaultBoardId)
    })
})
