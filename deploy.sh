echo "logging in..."
heroku container:login

echo "build Frontend"
docker build -t toomanyteams ./Frontend

echo "deploy frontend"
docker tag toomanyteams registry.heroku.com/toomanyteams/web
docker push registry.heroku.com/toomanyteams/web
heroku container:release web --app=toomanyteams

echo "build Backend"
docker build -t toomanyteams-socket ./Backend

echo "deploy Backend"
docker tag toomanyteams-socket registry.heroku.com/toomanyteams-socket/web
docker push registry.heroku.com/toomanyteams-socket/web
heroku container:release web --app=toomanyteams-socket
