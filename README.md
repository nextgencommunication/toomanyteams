# TooManyTams

Chatprogramm<br>
Developed by Jan, Maurice, Kevin and Lennart as a school project.<br>
Is running on: https://toomanyteams.herokuapp.com

## Setup

install required libraries with `npm install --include=dev` in both, Frontend and Backend folder.

run: `npm start` in both, Frontend and Backend folder.

## ENV

PORT: The port, the app uses suplies<br>
ORIGIN: https://toomanyteams.herokuapp.com<br>
SOCKET_URI: https://toomanyteams-socket.herokuapp.com, defined in environment*.ts

## Deployment

The Deployment is a manually process currently.

### Frontend

https://toomanyteams.herokuapp.com

build:<br>
`docker build -t toomanyteams ./Frontend`

test:<br>
`docker run -p 80:80 -e PORT=80 --name toomanyteams-container toomanyteams`

deploy:<br>
`heroku container:login`<br>
`docker tag toomanyteams registry.heroku.com/toomanyteams/web`<br>
`docker push registry.heroku.com/toomanyteams/web`<br>
`heroku container:release web --app=toomanyteams`

open in Browser:<br>
`heroku open --app toomanyteams`

### Backend

https://toomanyteams-socket.herokuapp.com

build:<br>
`docker build -t toomanyteams-socket ./Backend`

test:<br>
`docker run -p 80:80 -e PORT=80 --name toomanyteams-socket-container toomanyteams-socket`

deploy:<br>
`heroku container:login`<br>
`docker tag toomanyteams-socket registry.heroku.com/toomanyteams-socket/web`<br>
`docker push registry.heroku.com/toomanyteams-socket/web`<br>
`heroku container:release web --app=toomanyteams-socket`

open in Browser:<br>
`heroku open --app toomanyteams-socket`
